<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'sanyo-cms-dummy' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'sf_developer' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'VZ5Cc49fa|U+VpSdZ_cq-*VLQNCkYXMeDQ+22S9N' );

/** MySQL のホスト名 */
define( 'DB_HOST', '127.0.0.1' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'h~YK-Hl|nTMCg)xlEXRhef5aQNCi@5UZzok8E4-2Cp9SURLL7olUox}aeWp~65Av' );
define( 'SECURE_AUTH_KEY',  '3`8m!H$`UKJnZ?<r4#^U>gpuQHcHCWE[yJv4(,e#9sdjX6xuds!{N+:}y{AXyb[`' );
define( 'LOGGED_IN_KEY',    '!n(u5 91]?ycIJ|Vxag&RU^*X&mt+XbAGg[#[of],|T2g*9g]rdj[}4xm+_ve;h}' );
define( 'NONCE_KEY',        '1)3~ofEA*3bsvx.H2%r,`TDSmU`1}rs5.prDw{mTxnku7J+MI1kFl!3ONjDAkGfP' );
define( 'AUTH_SALT',        'O|hf]4^j$7):Tk|u`7-qM&d~n%y nhR0s[V*8ql1,A3sdV}?vfgS%`d?qy&q`7x?' );
define( 'SECURE_AUTH_SALT', '|mJsvSd+&)o@e9q-S>V>>#UOM?4{ZvzF~lpx;s{Tj/uc1_yH@N/@m)0/gFVSo_?*' );
define( 'LOGGED_IN_SALT',   'J)~/2} 5k4I.>FmYEByn@1F)32P~^d# 5z_!RfH47&t3gR-$etESTvyb4VDB:LRh' );
define( 'NONCE_SALT',       'wBfA{mkCex]p!]q(,*q@MGgA!x;mtZRXUJH%jwC<bm/]V~/$$cT^LH/}%6iScqRS' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
