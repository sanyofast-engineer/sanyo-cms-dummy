<?php
if ( !is_admin() ) {
	function add_cssfiles() {
   	 // サイト共通のCSSの読み込み
  	  wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css');
	  wp_enqueue_style( 'jquery.responsive-nav', get_stylesheet_directory_uri() . '/css/jquery.responsive-nav.css', array()/*, date('YmdHis', getlastmod(get_template_directory() . '/css/jquery.responsive-nav.css'))*/ );
 	  wp_enqueue_style( 'slider-pro', get_stylesheet_directory_uri() . '/css/slider-pro.css', array() );
    wp_enqueue_style( 'fatNav', get_stylesheet_directory_uri() . '/css/jquery.fatNav.css', array() );
    wp_enqueue_style( 'temify_customcss', get_stylesheet_directory_uri() . '/css/temify_custom.css', array('moto_style') );
  	  wp_enqueue_style( 'vt_custom', get_stylesheet_directory_uri() . '/vt_custom.css', array('theme-style'));
   	 //wp_enqueue_style( 'maximagecss', get_stylesheet_directory_uri() . '/css/jquery.maximage.css', array(), date('YmdHis', getlastmod(get_template_directory()   . '/css/jquery.maximage.css')) );
	}
	function add_jsfiles() {
	    // サイト共通のJSの読み込み
	    wp_enqueue_script( 'jquery.responsive-nav', get_stylesheet_directory_uri() . '/js/jquery.responsive-nav.js', array(), '1.0.0', true );
	    wp_enqueue_script( 'jquerysliderPro', get_stylesheet_directory_uri() . '/js/jquery.sliderPro.js', array(), '1.0.0', true );
	    wp_enqueue_script( 'jquery.overflowScroll', get_stylesheet_directory_uri() . '/js/jquery.overflowScroll.js', array(), '1.0.0', true );
      wp_enqueue_script( 'jquery.fatNav', get_stylesheet_directory_uri() . '/js/jquery.fatNav.js', array(), '1.0.0', true );
	    //wp_enqueue_script( 'maximagejs', get_stylesheet_directory_uri() . '/js/jquery.maximage.js', array(), '1.0.0', true );
	    //wp_enqueue_script( 'jquery.japan-map', get_stylesheet_directory_uri() . '/js/jquery.japan-map.min.js', array(), '1.0.0', true );
	}
	add_action( 'wp_enqueue_scripts', 'add_cssfiles' );
	add_action( 'wp_enqueue_scripts', 'add_jsfiles' );
}

remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head');
// Since 4.4
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','rest_output_link_wp_head');
//add_filter( 'author_rewrite_rules', '__return_empty_array' );
//remove_filter( 'themify_builder_module_content', 'wpautop' );


//'月・日・年'のデフォルト並びを'年・月・日’に変更
add_filter('themify_loop_date', 'custom_themify_loop_date');
function custom_themify_loop_date($date){
    return get_option('date_format');
}


//is_mobileの再定義
function is_mobile(){
  $useragents = array(
    'iPhone',          // iPhone
    'iPod',            // iPod touch
    'Android',         // 1.5+ Android
    'dream',           // Pre 1.5 Android
    'CUPCAKE',         // 1.5+ Android
    'blackberry9500',  // Storm
    'blackberry9530',  // Storm
    'blackberry9520',  // Storm v2
    'blackberry9550',  // Storm v2
    'blackberry9800',  // Torch
    'webOS',           // Palm Pre Experimental
    'incognito',       // Other iPhone browser
    'webmate'          // Other iPhone browser
  );
  $pattern = '/'.implode('|', $useragents).'/i';
  return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}

//TinyMCE Advanceにスタイルを追加
function plugin_mce_css( $mce_css ) {
    if ( ! empty( $mce_css ) )
        $mce_css .= ',';

    $font_url = get_stylesheet_directory_uri().'/editor-style.css';
    $mce_css .= str_replace( ',', '%2C', $font_url );

    return $mce_css;
}
add_filter( 'mce_css', 'plugin_mce_css' );
//add_editor_style('editor-style.css');
function my_mce4_options( $init ) {
$default_colors = '
    "000000", "Black",
    "993300", "Burnt orange",
    "333300", "Dark olive",
    "003300", "Dark green",
    "003366", "Dark azure",
    "000080", "Navy Blue",
    "333399", "Indigo",
    "333333", "Very dark gray",
    "800000", "Maroon",
    "FF6600", "Orange",
    "808000", "Olive",
    "008000", "Green",
    "008080", "Teal",
    "0000FF", "Blue",
    "666699", "Grayish blue",
    "808080", "Gray",
    "FF0000", "Red",
    "FF9900", "Amber",
    "99CC00", "Yellow green",
    "339966", "Sea green",
    "33CCCC", "Turquoise",
    "3366FF", "Royal blue",
    "800080", "Purple",
    "999999", "Medium gray",
    "FF00FF", "Magenta",
    "FFCC00", "Gold",
    "FFFF00", "Yellow",
    "00FF00", "Lime",
    "00FFFF", "Aqua",
    "00CCFF", "Sky blue",
    "993366", "Brown",
    "C0C0C0", "Silver",
    "FF99CC", "Pink",
    "FFCC99", "Peach",
    "FFFF99", "Light yellow",
    "CCFFCC", "Pale green",
    "CCFFFF", "Pale cyan",
    "99CCFF", "Light sky blue",
    "CC99FF", "Plum",
    "FFFFFF", "White"
    ';
$custom_colors = '
    "ce2c17", "独自red",
    "3c258d", "独自blue",
    "ff5c86", "独自pink",
    "ef3e00", "独自オレンジ",
    "59ab95", "独自グリーン",
    "48a722", "独自yグリーン",
    "00d96e", "独自Eグリーン",
    "2eacad", "独自mグリーン",
    "69c0f6", "独自skyblue",
    "624525", "brown"
    ';
$style_formats = array(
        array(
            'title' => '余白無し',
            'block' => 'p',
            'classes' => 'mb0'
        ),
        array(
            'title' => '太文字',
            'inline' => 'b'
        ),
    );
$init['style_formats'] = json_encode($style_formats);
$init['textcolor_map'] = '['.$default_colors.','.$custom_colors.']';
$init['textcolor_rows'] = 6; // 色を最大何行まで表示させるか
$init['textcolor_cols'] = 10; // 色を最大何列まで表示させるか
$init['fontsize_formats'] = '10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 32px 33px 35px 38px 40px 46px';

$init['body_class'] = 'editor-area';

return $init;
}
add_filter('tiny_mce_before_init', 'my_mce4_options');

/**
* shortcodeがpタグに囲まれるfix
*
*/
function shortcode_empty_paragraph_fix($content) {
    $array = array (
        '<p>[' => '[',
        ']</p>' => ']',
        ']<br />' => ']'
    );
 
    $content = strtr($content, $array);
    return $content;
}

add_filter('themify_builder_module_content', 'shortcode_empty_paragraph_fix');


//tinymceのスタイルでimgをpタグでくくらないようにする
function remove_p_on_images($content){
    return preg_replace('/<p>(\s*)(<img .* \/>)(\s*)<\/p>/iU', '\2', $content);
}
add_filter('themify_builder_module_content', 'remove_p_on_images');
add_filter('the_content', 'remove_p_on_images');

//wordpressの更新通知オフ
add_filter( 'auto_core_update_send_email', '__return_false' );

//管理者以外でwordpressの更新通知を管理画面上から消す
if( !current_user_can( 'administrator' ) ) {
 add_filter( 'pre_site_transient_update_core', '__return_zero' );
 remove_action( 'wp_version_check', 'wp_version_check' );
 remove_action( 'admin_init', '_maybe_update_core' );
}

//カテゴリーの順番が変わらないようにする
function solecolor_wp_terms_checklist_args( $args, $post_id ){
   if ( $args['checked_ontop'] !== false ){
        $args['checked_ontop'] = false;
   }
   return $args;
}
add_filter('wp_terms_checklist_args', 'solecolor_wp_terms_checklist_args',10,2);

//サブタイトルを表示させるナビ用
/*add_filter('walker_nav_menu_start_el', 'description_in_nav_menu', 10, 4);

function description_in_nav_menu($item_output, $item){
    return preg_replace('/(<a.*?>[^<]*?)</', '$1' . "<br /><span>{$item->attr_title}</span><", $item_output);
}*/
/*
function add_nav_menu_custom_class( $sorted_menu_items ) {
    $num = 1;
    foreach ( $sorted_menu_items as $key => $sorted_menu_item ) {
    $sorted_menu_items[$key]->classes[] = 'nav' . $num;
    if ( $num == 1 ) {
    $sorted_menu_items[$key]->classes[] = 'menu-item-first';
    } elseif ( $num == count( $sorted_menu_items ) ) {
    $sorted_menu_items[$key]->classes[] = 'menu-item-last';
    }
    $num++;
    }
    return $sorted_menu_items;
}
add_filter( 'wp_nav_menu_objects', 'add_nav_menu_custom_class' );
*/

//ログイン画面のロゴ
function login_logo_image() {
    echo '<style type="text/css">
    				body.login{ background: black; }
            #login h1 a {
                background-image: url(' . themify_logo_image2() . ') !important;
                background-size: contain;
                width: 100%;
            }
    </style>';
}
add_action('login_head', 'login_logo_image');

function themify_logo_image2( $location = 'site_logo', $cssid = 'site-logo' ) {
    global $themify_customizer;

    $logo_tag = apply_filters( 'themify_' . $location . '_logo_tag', 'div' );
    $logo_mod = get_theme_mod( $cssid . '_image' );
    $logo_is_image = themify_get( 'setting-' . $location ) == 'image' && themify_check( 'setting-' . $location . '_image_value' );

        $type = 'customizer';
        //$html .= $logo_mod;
        $itou = explode('"', $logo_mod);
        //var_dump($itou);
        $html .= $itou[9];

    return apply_filters( 'themify_' . $location . '_logo_html', $html, $location, $logo_tag, $type );
}

//function new_excerpt_more($post) {
//     return '<a class="more_links" href="'. get_permalink($post->ID) . '">' . '続きを読む' . '</a>';
//}
//add_filter('excerpt_more', 'new_excerpt_more');


/////////////////////// メタボックスの追加と保存 ///////////////////////

add_action("admin_init", "metaboxs_init3");
function metaboxs_init3(){ // 編集画面にメタボックスを追加する
    add_meta_box( 'myupload3', '全画面背景', 'myupload_postmeta3', 'page', 'side','low' ); // ポジションはsideが推奨です
    add_action('save_post', 'save_myupload_postmeta3');
}

add_action("admin_init", "metaboxs_init2");
function metaboxs_init2(){ // 編集画面にメタボックスを追加する
    add_meta_box( 'myupload2', 'メイン背景', 'myupload_postmeta2', 'page', 'side','low' ); // ポジションはsideが推奨です
    add_action('save_post', 'save_myupload_postmeta2');
}
  
add_action("admin_init", "metaboxs_init");
function metaboxs_init(){ // 編集画面にメタボックスを追加する
    add_meta_box( 'myupload', 'メイン画像', 'myupload_postmeta', 'page', 'side','high' ); // ポジションはsideが推奨です
    add_action('save_post', 'save_myupload_postmeta');
}

  
/////////////////////// メタボックス（画像アップロード用） /////////////////////// 
   
function myupload_postmeta(){ //ページに表示されるカスタムフィールド
    global $post;
    $post_id = $post->ID;
    $myupload_images = get_post_meta( $post_id, 'myupload_images', true );
    foreach((array)$myupload_images as $key => $img_id ){
        $thumb_src = wp_get_attachment_image_src ($img_id,'thumbnail');
        if ( empty ($thumb_src[0]) ){ //画像が存在しない空IDを強制的に取り除く
            delete_post_meta( $post_id, 'myupload_images', $img_id );
        } else {
            $myupload_li.= 
            "\t".'<li class="img" id="img_'.$img_id.'">'."\n".
            "\t\t".'<span class="img_wrap">'."\n".
            "\t\t\t".'<a href="#" class="myupload_images_remove" title="画像を削除する"></a>'."\n".
            "\t\t\t".'<img src="'.$thumb_src[0].'"/>'."\n".
            "\t\t\t".'<input type="hidden" name="myupload_images[]" value="'.$img_id.'" />'."\n".
            "\t\t".'</span>'."\n".
            "\t".'</li>'."\n";
        }
    }
?>
<style type="text/css">
    #myupload_images { display:block; clear:both; list-style-type: none; }
    #myupload_images:after { content:"."; display:block; height:0; clear:both; visibility:hidden; }
    #myupload_images li { display:block; width:100%; margin:0; padding:5px 0; text-align:center; }
    #myupload_images li span.img_wrap { display:inline-block; margin:0; height:auto; width:auto; padding:4px; position:relative; background:#ccc; }
    #myupload_images li span img { margin:0; padding:0; max-height: 160px; width:auto; vertical-align:text-bottom; }
    #myupload_images li span input { display:none; }
    #myupload_images li span a.myupload_images_remove { position:absolute; top:-8px; right:-8px; height:32px; width:32px; text-align:center; vertical-align:middle; background:#ccc; border-radius:50%; }
    #myupload_images li span a.myupload_images_remove:before { content:'×'; display:inline-block; text-decoration:none; width:1em; margin-right:.2em; text-align:center; font-size:20px; line-height:20px; padding:6px; color:#fff; font-weight:bold; }
    #myupload_images li span a.myupload_images_remove:hover { background:#aaa; }
    #myupload_buttons a, #myupload_buttons input { padding:10px 15px; height:40px; line-height:20px; font-weight:bold; text-align:center; }
</style>
<div id="myupload_buttons">
    <a id="myupload_media" type="button" class="button" title="画像を追加・変更">アイテム画像の追加・削除</a>
    <input type="submit" name="save" id="save2" class="button" value="更新" />
    <p>ドラッグで好きな順に並べてください。</p>
</div>
<ul id="myupload_images">
<?php echo $myupload_li; ?>
</ul>
<input type="hidden" name="myupload_postmeta_nonce" value="<?php echo wp_create_nonce(basename(__FILE__)); ?>" />
<script type="text/javascript">
jQuery( function(){
    var custom_uploader;
    jQuery('#myupload_media').click(function(e) {
        e.preventDefault();
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }
        custom_uploader = wp.media({
            title: _wpMediaViewsL10n.mediaLibraryTitle,
            library: {
                type: 'image'
            },
            button: {
                text: '画像を選択'
            },
            multiple: true, // falseのとき画像選択は一つのみ可能
            frame: 'select', // select | post. selectは左のnavを取り除く指定
            editing:   false,
        }); 
  
 
        custom_uploader.on('ready', function() {
           // jQuery('select.attachment-filters [value="uploaded"]').attr( 'selected', true ).parent().trigger('change');
            //「この投稿への画像」をデフォルト表示　不要ならコメントアウト
        });
        custom_uploader.on('select', function() {
            var images = custom_uploader.state().get('selection'),
                ex_ul = jQuery('#myupload_images'),
                ex_id = 0, 
                array_ids = [];
            if ( ex_ul[0] ){ //すでに登録されている画像を配列に格納
                ex_ul.children('li').each( function( ){
                    ex_id = Number(jQuery(this).attr( 'id' ).slice(4));
                    array_ids.push( ex_id );
                });
            }
            console.log(images); 
            images.each(function( file ){
                new_id = file.toJSON().id;
                if ( jQuery.inArray( new_id, array_ids ) > -1 ){ //投稿編集画面のリストに重複している場合、削除
                    ex_ul.find('li#img_'+ new_id).remove();
                }
                ex_ul.append('<li class="img" id=img_'+ new_id +'></li>').find('li:last').append(
                    '<span class="img_wrap">' + 
                    '<a href="#" class="myupload_images_remove" title="画像を削除する"></a>' +
                    '<img src="'+file.attributes.sizes.thumbnail.url+'" />' +
                    '<input type="hidden" name="myupload_images[]" value="'+ new_id +'" />' + 
                    '</span>'
                );
            });
        });
        custom_uploader.open();
    });
    jQuery( ".myupload_images_remove" ).live( 'click', function( e ) {
        e.preventDefault();
        e.stopPropagation();
        img_obj = jQuery(this).parents('li.img').remove();
    });
    jQuery( "#myupload_images" ).sortable({
        axis : 'y',
        cursor : "move",
        tolerance : "pointer",
        opacity: 0.6
    });
});
</script>
 
 
<?php }
   
/*データ更新時の保存*/
function save_myupload_postmeta( $post_id ){
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
    return $post_id; // 自動保存ルーチンの時は何もしない
    if (!wp_verify_nonce($_POST['myupload_postmeta_nonce'], basename(__FILE__)))
    return $post_id; // wp-nonceチェック
    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) ) // パーミッションチェック
        return $post_id;
    } else {
        if ( !current_user_can( 'edit_post', $post_id ) ) // パーミッションチェック
        return $post_id;
    }
    $new_images = isset($_POST['myupload_images']) ? $_POST['myupload_images']: null; //POSTされたデータ
    $ex_images = get_post_meta( $post_id, 'myupload_images', true ); //DBのデータ
    if ( $ex_images !== $new_images ){
        if ( $new_images ){
            update_post_meta( $post_id, 'myupload_images', $new_images ); // アップデート
        } else {
            delete_post_meta( $post_id, 'myupload_images', $ex_images ); 
        }
    }
}


/////////////////////// メタボックス（背景画像アップロード用） /////////////////////// 
   
function myupload_postmeta2(){ //ページに表示されるカスタムフィールド
    global $post;
    $post_id2 = $post->ID;
    $myupload_images2 = get_post_meta( $post_id2, 'myupload_images2', true );
    foreach((array)$myupload_images2 as $key => $img_id2 ){
        $thumb_src2 = wp_get_attachment_image_src ($img_id2,'thumbnail');
        if ( empty ($thumb_src2[0]) ){ //画像が存在しない空IDを強制的に取り除く
            delete_post_meta( $post_id2, 'myupload_images2', $img_id2 );
        } else {
            $myupload_li2.= 
            "\t".'<li class="img" id="img_'.$img_id2.'">'."\n".
            "\t\t".'<span class="img_wrap">'."\n".
            "\t\t\t".'<a href="#" class="myupload_images2_remove" title="画像を削除する"></a>'."\n".
            "\t\t\t".'<img src="'.$thumb_src2[0].'"/>'."\n".
            "\t\t\t".'<input type="hidden" name="myupload_images2[]" value="'.$img_id2.'" />'."\n".
            "\t\t".'</span>'."\n".
            "\t".'</li>'."\n";
        }
    }
?>
<style type="text/css">
    #myupload_images2 { display:block; clear:both; list-style-type: none; }
    #myupload_images2:after { content:"."; display:block; height:0; clear:both; visibility:hidden; }
    #myupload_images2 li { display:block; width:100%; margin:0; padding:5px 0; text-align:center; }
    #myupload_images2 li span.img_wrap { display:inline-block; margin:0; height:auto; width:auto; padding:4px; position:relative; background:#ccc; }
    #myupload_images2 li span img { margin:0; padding:0; max-height: 160px; width:auto; vertical-align:text-bottom; }
    #myupload_images2 li span input { display:none; }
    #myupload_images2 li span a.myupload_images2_remove { position:absolute; top:-8px; right:-8px; height:32px; width:32px; text-align:center; vertical-align:middle; background:#ccc; border-radius:50%; }
    #myupload_images2 li span a.myupload_images2_remove:before { content:'×'; display:inline-block; text-decoration:none; width:1em; margin-right:.2em; text-align:center; font-size:20px; line-height:20px; padding:6px; color:#fff; font-weight:bold; }
    #myupload_images2 li span a.myupload_images2_remove:hover { background:#aaa; }
    #myupload_buttons2 a, #myupload_buttons2 input { padding:10px 15px; height:40px; line-height:20px; font-weight:bold; text-align:center; }
</style>
<div id="myupload_buttons2">
    <a id="myupload_media2" type="button" class="button" title="画像を追加・変更">アイテム画像の追加・削除</a>
    <input type="submit" name="save3" id="save4" class="button" value="更新" />
    <p>ドラッグで好きな順に並べてください。</p>
</div>
<ul id="myupload_images2">
<?php echo $myupload_li2; ?>
</ul>
<input type="hidden" name="myupload_postmeta_nonce2" value="<?php echo wp_create_nonce(basename(__FILE__)); ?>" />
<script type="text/javascript">
jQuery( function(){
    var custom_uploader2;
    jQuery('#myupload_media2').click(function(e) {
        e.preventDefault();
        if (custom_uploader2) {
            custom_uploader2.open();
            return;
        }
        custom_uploader2 = wp.media({
            title: _wpMediaViewsL10n.mediaLibraryTitle,
            library: {
                type: 'image'
            },
            button: {
                text: '画像を選択'
            },
            multiple: true, // falseのとき画像選択は一つのみ可能
            frame: 'select', // select | post. selectは左のnavを取り除く指定
            editing:   false,
        }); 
  
 
        custom_uploader2.on('ready', function() {
           // jQuery('select.attachment-filters [value="uploaded"]').attr( 'selected', true ).parent().trigger('change');
            //「この投稿への画像」をデフォルト表示　不要ならコメントアウト
        });
        custom_uploader2.on('select', function() {
            var images = custom_uploader2.state().get('selection'),
                ex_ul = jQuery('#myupload_images2'),
                ex_id = 0, 
                array_ids = [];
            if ( ex_ul[0] ){ //すでに登録されている画像を配列に格納
                ex_ul.children('li').each( function( ){
                    ex_id = Number(jQuery(this).attr( 'id' ).slice(4));
                    array_ids.push( ex_id );
                });
            }
            console.log(images); 
            images.each(function( file ){
                new_id = file.toJSON().id;
                if ( jQuery.inArray( new_id, array_ids ) > -1 ){ //投稿編集画面のリストに重複している場合、削除
                    ex_ul.find('li#img_'+ new_id).remove();
                }
                ex_ul.append('<li class="img" id=img_'+ new_id +'></li>').find('li:last').append(
                    '<span class="img_wrap">' + 
                    '<a href="#" class="myupload_images2_remove" title="画像を削除する"></a>' +
                    '<img src="'+file.attributes.sizes.thumbnail.url+'" />' +
                    '<input type="hidden" name="myupload_images2[]" value="'+ new_id +'" />' + 
                    '</span>'
                );
            });
        });
        custom_uploader2.open();
    });
    jQuery( ".myupload_images2_remove" ).live( 'click', function( e ) {
        e.preventDefault();
        e.stopPropagation();
        img_obj = jQuery(this).parents('li.img').remove();
    });
    jQuery( "#myupload_images2" ).sortable({
        axis : 'y',
        cursor : "move",
        tolerance : "pointer",
        opacity: 0.6
    });
});
</script>
 
 
<?php }
   
/*データ更新時の保存*/
function save_myupload_postmeta2( $post_id2 ){
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
    return $post_id2; // 自動保存ルーチンの時は何もしない
    if (!wp_verify_nonce($_POST['myupload_postmeta_nonce2'], basename(__FILE__)))
    return $post_id2; // wp-nonceチェック
    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id2 ) ) // パーミッションチェック
        return $post_id2;
    } else {
        if ( !current_user_can( 'edit_post', $post_id2 ) ) // パーミッションチェック
        return $post_id2;
    }
    $new_images2 = isset($_POST['myupload_images2']) ? $_POST['myupload_images2']: null; //POSTされたデータ
    $ex_images2 = get_post_meta( $post_id2, 'myupload_images2', true ); //DBのデータ
    if ( $ex_images2 !== $new_images2 ){
        if ( $new_images2 ){
            update_post_meta( $post_id2, 'myupload_images2', $new_images2 ); // アップデート
        } else {
            delete_post_meta( $post_id2, 'myupload_images2', $ex_images2 ); 
        }
    }
}

/////////////////////// メタボックス（全画面画像アップロード用） /////////////////////// 
   
function myupload_postmeta3(){ //ページに表示されるカスタムフィールド
    global $post;
    $post_id3 = $post->ID;
    $myupload_images3 = get_post_meta( $post_id3, 'myupload_images3', true );
    foreach((array)$myupload_images3 as $key => $img_id3 ){
        $thumb_src3 = wp_get_attachment_image_src ($img_id3,'thumbnail');
        if ( empty ($thumb_src3[0]) ){ //画像が存在しない空IDを強制的に取り除く
            delete_post_meta( $post_id3, 'myupload_images3', $img_id3 );
        } else {
            $myupload_li3.= 
            "\t".'<li class="img" id="img_'.$img_id3.'">'."\n".
            "\t\t".'<span class="img_wrap">'."\n".
            "\t\t\t".'<a href="#" class="myupload_images3_remove" title="画像を削除する"></a>'."\n".
            "\t\t\t".'<img src="'.$thumb_src3[0].'"/>'."\n".
            "\t\t\t".'<input type="hidden" name="myupload_images3[]" value="'.$img_id3.'" />'."\n".
            "\t\t".'</span>'."\n".
            "\t".'</li>'."\n";
        }
    }
?>
<style type="text/css">
    #myupload_images3 { display:block; clear:both; list-style-type: none; }
    #myupload_images3:after { content:"."; display:block; height:0; clear:both; visibility:hidden; }
    #myupload_images3 li { display:block; width:100%; margin:0; padding:5px 0; text-align:center; }
    #myupload_images3 li span.img_wrap { display:inline-block; margin:0; height:auto; width:auto; padding:4px; position:relative; background:#ccc; }
    #myupload_images3 li span img { margin:0; padding:0; max-height: 160px; width:auto; vertical-align:text-bottom; }
    #myupload_images3 li span input { display:none; }
    #myupload_images3 li span a.myupload_images3_remove { position:absolute; top:-8px; right:-8px; height:32px; width:32px; text-align:center; vertical-align:middle; background:#ccc; border-radius:50%; }
    #myupload_images3 li span a.myupload_images3_remove:before { content:'×'; display:inline-block; text-decoration:none; width:1em; margin-right:.2em; text-align:center; font-size:20px; line-height:20px; padding:6px; color:#fff; font-weight:bold; }
    #myupload_images3 li span a.myupload_images3_remove:hover { background:#aaa; }
    #myupload_buttons3 a, #myupload_buttons3 input { padding:10px 15px; height:40px; line-height:20px; font-weight:bold; text-align:center; }
</style>
<div id="myupload_buttons3">
    <a id="myupload_media3" type="button" class="button" title="画像を追加・変更">アイテム画像の追加・削除</a>
    <input type="submit" name="save5" id="save6" class="button" value="更新" />
    <p>ドラッグで好きな順に並べてください。</p>
</div>
<ul id="myupload_images3">
<?php echo $myupload_li3; ?>
</ul>
<input type="hidden" name="myupload_postmeta_nonce3" value="<?php echo wp_create_nonce(basename(__FILE__)); ?>" />
<script type="text/javascript">
jQuery( function(){
    var custom_uploader3;
    jQuery('#myupload_media3').click(function(e) {
        e.preventDefault();
        if (custom_uploader3) {
            custom_uploader3.open();
            return;
        }
        custom_uploader3 = wp.media({
            title: _wpMediaViewsL10n.mediaLibraryTitle,
            library: {
                type: 'image'
            },
            button: {
                text: '画像を選択'
            },
            multiple: true, // falseのとき画像選択は一つのみ可能
            frame: 'select', // select | post. selectは左のnavを取り除く指定
            editing:   false,
        }); 
  
 
        custom_uploader3.on('ready', function() {
           // jQuery('select.attachment-filters [value="uploaded"]').attr( 'selected', true ).parent().trigger('change');
            //「この投稿への画像」をデフォルト表示　不要ならコメントアウト
        });
        custom_uploader3.on('select', function() {
            var images = custom_uploader3.state().get('selection'),
                ex_ul = jQuery('#myupload_images3'),
                ex_id = 0, 
                array_ids = [];
            if ( ex_ul[0] ){ //すでに登録されている画像を配列に格納
                ex_ul.children('li').each( function( ){
                    ex_id = Number(jQuery(this).attr( 'id' ).slice(4));
                    array_ids.push( ex_id );
                });
            }
            console.log(images); 
            images.each(function( file ){
                new_id = file.toJSON().id;
                if ( jQuery.inArray( new_id, array_ids ) > -1 ){ //投稿編集画面のリストに重複している場合、削除
                    ex_ul.find('li#img_'+ new_id).remove();
                }
                ex_ul.append('<li class="img" id=img_'+ new_id +'></li>').find('li:last').append(
                    '<span class="img_wrap">' + 
                    '<a href="#" class="myupload_images3_remove" title="画像を削除する"></a>' +
                    '<img src="'+file.attributes.sizes.thumbnail.url+'" />' +
                    '<input type="hidden" name="myupload_images3[]" value="'+ new_id +'" />' + 
                    '</span>'
                );
            });
        });
        custom_uploader3.open();
    });
    jQuery( ".myupload_images3_remove" ).live( 'click', function( e ) {
        e.preventDefault();
        e.stopPropagation();
        img_obj = jQuery(this).parents('li.img').remove();
    });
    jQuery( "#myupload_images3" ).sortable({
        axis : 'y',
        cursor : "move",
        tolerance : "pointer",
        opacity: 0.6
    });
});
</script>
 
 
<?php }
   
/*データ更新時の保存*/
function save_myupload_postmeta3( $post_id3 ){
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
    return $post_id3; // 自動保存ルーチンの時は何もしない
    if (!wp_verify_nonce($_POST['myupload_postmeta_nonce3'], basename(__FILE__)))
    return $post_id3; // wp-nonceチェック
    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id3 ) ) // パーミッションチェック
        return $post_id3;
    } else {
        if ( !current_user_can( 'edit_post', $post_id3 ) ) // パーミッションチェック
        return $post_id3;
    }
    $new_images3 = isset($_POST['myupload_images3']) ? $_POST['myupload_images3']: null; //POSTされたデータ
    $ex_images3 = get_post_meta( $post_id3, 'myupload_images3', true ); //DBのデータ
    if ( $ex_images3 !== $new_images3 ){
        if ( $new_images3 ){
            update_post_meta( $post_id3, 'myupload_images3', $new_images3 ); // アップデート
        } else {
            delete_post_meta( $post_id3, 'myupload_images3', $ex_images3 ); 
        }
    }
}

/*ここまで*/



function post_output_css() {
    $pt = get_post_type();
    if ($pt == 'page') { //投稿の場合はpost
        $hide_postdiv_css = '<style type="text/css">#postdiv, #postdivrich { display: none; }</style>';
        echo $hide_postdiv_css;
    }
}
add_action('admin_head', 'post_output_css');

//retina非対応用
function disable_srcset( $sources ) { return false; }
add_filter( 'wp_calculate_image_srcset', 'disable_srcset' );



//祖先カテゴリーを取得する関数
function get_term_ancestor_by_term ( $sbjcat = '', $tax_name = 'category' ) { 
// [$sbjcat = カテゴリIDまたはスラッグ],
// [$tax_name = タクソノミ名]
    if ( empty ($sbjcat) ){
        return false; //カテゴリIDがセットされていなければfalseを返す
    } elseif ( !is_numeric( $sbjcat ) ) { 
        $get_sbjcat = get_term_by( 'slug', $sbjcat, $tax_name );
        if ( $get_sbjcat ) $sbjcat = $get_sbjcat->term_id; 
    }
    while ( 1 ) { // 親の存在しないカテゴリが出るまでループ
        $get_cat = get_term ( $sbjcat , $tax_name);
        $cat_parent_id = $get_cat->parent;
        if ( $cat_parent_id == 0 ) { 
            $reqcat = $get_cat;
            break;
        } else { 
            $sbjcat = $cat_parent_id;
        }
    }
    return $reqcat;
}

function get_term_ancestor ( $post_id = '', $extree = '', $tax_name = 'category' ) { 
// [$post_id = 投稿ID],
// [$extree = 含めないカテゴリツリーのIDまたはスラッグ],
// [$tax_name = タクソノミ名]
    if ( $extree && !is_numeric( $extree ) ) { 
        $get_extree = get_term_by( 'slug', $extree, $tax_name );
        if ( $get_extree ) $extree = $get_extree->term_id; 
    }
    if ( !$post_id ) { 
        global $post;
        $post_id = $post->ID;
    }
    $terms = get_the_terms( $post_id, $tax_name );
    if ($terms) : foreach ( $terms as $current_term ) : 
        if ( $current_term->term_id !== $extree && !term_is_ancestor_of ( $extree, $current_term->term_id, $tax_name ) ) { // $extreeが親ではない場合
            $req_id = $current_term->term_id;
            break;
        }
    endforeach; endif;
    if ( $req_id ){ 
        return get_term_ancestor_by_term( $req_id, $tax_name );
    } else { 
        return false;
    }
}


//会員ページかどうかの判定
function is_member_page($page_name = 'usces-member') {
    global $usces;
    if($usces->item->post_name == $page_name) return true;
    return false;
}

//カートページかどうかの判定
function is_cart_page($page_name = 'usces-cart') {
    global $usces;
    if($usces->item->post_name == $page_name) return true;
    return false;
}

function is_ans_cat_item(){
    $ancestor = get_term_ancestor_by_term ( get_query_var('cat') );
    $slug = $ancestor->slug;
    if($slug == 'item') return true;
    return false;
}

function is_mime_type_selector($type){
    global $post;
    $mime_t = $post->post_mime_type;
    if($mime_t == $type) return true;
    return false;
}

//プラグインの有効無効判定
function is_active_plugin($path){
    $active_plugins = get_option('active_plugins');
    if(is_array($active_plugins)) {
        foreach($active_plugins as $value){
            if( $value == $path) return true;
        }
    }
    return false;
}







//welcart用
//Scripts and Style sheets
function blanc_scripts(){
    global $post;
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'modernizr', get_stylesheet_directory_uri() .'/js/vendor/modernizr.js', array(), '', true );
    //wp_enqueue_script( 'scripts', get_stylesheet_directory_uri() .'/js/scripts.js', array(), '1.0', true );
    wp_enqueue_style( 'moto_style', get_stylesheet_directory_uri() .'/css/moto_style.css', 'all' );
    if( is_singular() && comments_open() ){
        wp_enqueue_script('comment-reply');
    }

    if ( is_front_page() || ( is_single() && ( isset($post->post_mime_type) && $post->post_mime_type == 'item' ) ) ){
        wp_enqueue_style( 'flexslider-css', get_stylesheet_directory_uri() .'/css/flexslider.css' );
        wp_enqueue_script( 'flexslider-js', get_stylesheet_directory_uri() .'/js/jquery.flexslider-min.js', array(), '2.4.0', true );
        if ( is_front_page() ){
            //wp_enqueue_script( 'use-flexslider-frontpage', get_stylesheet_directory_uri() .'/js/use-flexslider-frontpage.js', array(), '1.0', true );
        }
    }
    if( is_single() && is_mime_type_selector('item') ){
        wp_enqueue_style( 'swipebox-style', get_stylesheet_directory_uri() .'/css/swipebox.min.css', 'all' );
        wp_enqueue_script( 'swipebox', get_stylesheet_directory_uri() .'/js/jquery.swipebox.min.js', array(), '1.4.1', true );
        wp_enqueue_script( 'use-swipebox', get_stylesheet_directory_uri() .'/js/use-swipebox.js', array(), '1.0', true );
        if( isset($post->post_mime_type) && $post->post_mime_type == 'item' ){
            wp_enqueue_script( 'scripts-item', get_stylesheet_directory_uri() .'/js/scripts-item.js', array(), '1.1', true);
        }
    }
    //Form Validation for Welcart e-commerce plugin
    if( function_exists('usces_the_item') ) {
        $usces_validation = array( 'newmemberform', 'member', 'customer', 'delivery' );
        global $usces;
        if( in_array( $usces->page, $usces_validation ) ){
            wp_enqueue_style( 'validationEngine-css', get_stylesheet_directory_uri() .'/css/validationEngine.jquery.css', 'all' );
            wp_enqueue_script( 'validationEngine', get_stylesheet_directory_uri() .'/js/jquery.validationEngine.js', array(), '2.6.2', true );
            $wp_lang = get_bloginfo( 'language' );
            switch( $wp_lang ){
            case 'zh_CN':
            case 'zh_TW':
            case 'pt_BR':
            $wp_lang = $wp_lang;
            break;
            case 'zh':
            $wp_lang = $wp_lang . '_CN';
            break;
            case 'cs_CZ':
            case 'nb_NO':
            case 'nn_NO':
            $wp_lang = substr( $wp_lang, -2 );
            break;
            default:
            $wp_lang = substr( $wp_lang, 0, 2 );
            }
            $jve_lang = '/js/languages/jquery.validationEngine-' . $wp_lang . '.js';
            if( file_exists( get_template_directory(). $jve_lang ) ){
                wp_enqueue_script( 'validationEngine-lang', get_stylesheet_directory_uri(). $jve_lang, array(), '', true );
            } else {
                wp_enqueue_script( 'validationEngine-lang', get_stylesheet_directory_uri() . '/js/languages/jquery.validationEngine-en.js', array(), '', true );
            }
                wp_enqueue_script( 'use-validationEngine', get_stylesheet_directory_uri() . '/js/use-validationEngine.js', array(), '1.0',  true );
            }
    }
    wp_enqueue_style( 'normalize-style', get_stylesheet_directory_uri() .'/css/normalize.css' );
    wp_enqueue_style( 'foundation-style', get_stylesheet_directory_uri() .'/css/foundation.min.css' );
    //wp_enqueue_style( 'blanc-style', get_stylesheet_uri() );
    wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() .'/css/font-awesome.min.css' );
    if( function_exists('usces_the_item') ){
        wp_enqueue_style( 'welcart-style', get_stylesheet_directory_uri() .'/css/welcart.css' );
    }
}
add_action( 'wp_enqueue_scripts', 'blanc_scripts' );



//Remove archive head title displayed by WordPress 4.1
function blanc_get_the_archive_title() {
    if ( is_category() ) {
        $title = sprintf( __( '%s' , 'blanc' ), single_cat_title( '', false ) );
    } elseif ( is_tag() ) {
        $title = sprintf( __( '%s' , 'blanc' ), single_tag_title( '', false ) );
    } elseif ( is_author() ) {
        $title = sprintf( __( '%s' , 'blanc' ), '<span class="vcard">' . get_the_author() . '</span>' );
    } elseif ( is_year() ) {
        $title = sprintf( __( '%s' , 'blanc' ), get_the_date( _x( 'Y', 'yearly archives date format', 'default' ) ) );
    } elseif ( is_month() ) {
        $title = sprintf( __( '%s' , 'blanc' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'default' ) ) );
    } elseif ( is_day() ) {
        $title = sprintf( __( '%s' , 'blanc' ), get_the_date( _x( 'F j, Y', 'daily archives date format', 'default' ) ) );
    } elseif ( is_tax( 'post_format' ) ) {
        if ( is_tax( 'post_format', 'post-format-aside' ) ) {
            $title = _x( 'Asides', 'post format archive title', 'default' );
        } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
            $title = _x( 'Galleries', 'post format archive title', 'default' );
        } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
            $title = _x( 'Images', 'post format archive title', 'default' );
        } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
            $title = _x( 'Videos', 'post format archive title', 'default' );
        } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
            $title = _x( 'Quotes', 'post format archive title', 'default' );
        } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
            $title = _x( 'Links', 'post format archive title', 'default' );
        } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
            $title = _x( 'Statuses', 'post format archive title', 'default' );
        } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
            $title = _x( 'Audio', 'post format archive title', 'default' );
        } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
            $title = _x( 'Chats', 'post format archive title', 'default' );
        }
    } elseif ( is_post_type_archive() ) {
        $title = sprintf( __( '<span>ARCHIVES</span> %s' , 'blanc' ), post_type_archive_title( '', false ) );
    } elseif ( is_tax() ) {
        $tax = get_taxonomy( get_queried_object()->taxonomy );
        /* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
        $title = sprintf( __( '%1$s: %2$s', 'default' ), $tax->labels->singular_name, single_term_title( '', false ) );
    } else {
        $title = __( '<span>ARCHIVES</span>', 'blanc' );
    }

    return $title;
}
add_filter('get_the_archive_title', 'blanc_get_the_archive_title', 10);

//******* Followings are the functions for Welcart e-commerce only *******
if( function_exists('usces_the_item')){
    
    //Change cart button to inquiry button when sku has no stock
    function blanc_single_sku_zaiko_message($inquery_button){
        $inquery_button = '<a href="'. home_url('usces-inquiry') .'" class="inquery_button skubutton"><i class="fa fa-envelope"></i>&nbsp;' .__('Inquiry Form', 'blanc') .'</a>';
        return $inquery_button;
    }
    add_filter('usces_filters_single_sku_zaiko_message', 'blanc_single_sku_zaiko_message', 10);
    add_filter('usces_filters_multi_sku_zaiko_message', 'blanc_single_sku_zaiko_message', 10);

    //Specific templates for item archives & item search page
    function blanc_category_item_template($category_item_template) {
        $category_id = get_query_var('cat');
        $parent_ids = get_ancestors($category_id, 'category');
        $parent_slugs = array();
        foreach( $parent_ids as $parent_id ){
            $parent = get_category($parent_id);
            $parent_slugs[] = $parent->slug;
        }
        if( in_array('item', $parent_slugs) || is_category('item') ){
            $category_item_template = dirname( __FILE__ ) . '/archive-item.php';
        }
        return $category_item_template;
    }
    add_filter( 'category_template', 'blanc_category_item_template' );

    function blanc_tag_item_template($tag_item_template) {
        global $post;
        if( isset($post->post_mime_type) && $post->post_mime_type == 'item' ) {
            $tag_item_template = dirname( __FILE__ ) . '/archive-item.php';
        }
        return $tag_item_template;
    }
    add_filter( 'tag_template', 'blanc_tag_item_template' );

    function blanc_search_item_template($search_item_template) {
        if( is_search() && !isset($_GET['searchitem']) ){
            $search_item_template = dirname( __FILE__ ) . '/search-item.php';
        }
        return $search_item_template;
    }
    add_filter( 'search_template', 'blanc_search_item_template' );

    //Change querys for item archives
    if( term_exists('item', 'category') ){
        function blanc_query($query){
            global $usces;
            $item_cat = get_category_by_slug('item');
            $item_cat_id = $item_cat->cat_ID;
            if ( is_admin() || ! $query->is_main_query() ){
                return;
            } elseif( $query->is_home() || $query->is_author() || $query->is_date() ){
                $query->set('category__not_in', $item_cat_id);
            } elseif( $query->is_category() ){
                $category_id = get_query_var('cat');
                $parent_ids = get_ancestors($category_id, 'category');
                $parent_slugs = array();
                foreach ($parent_ids as $parent_id){
                    $parent = get_category($parent_id);
                    $parent_slugs[] = $parent->slug;
                }
                if( in_array('item', $parent_slugs) || is_category('item') ){
                    $query->set('posts_per_page', '12');
                }
            } elseif ( $query->is_search && isset($_GET['searchitem']) ) {
                $query->set('category__not_in', $item_cat_id);
            } elseif ( $query->is_search && !isset($_GET['searchitem']) ){
                $query->set('posts_per_page','12');
                $query->set('category_name','item');
            }
        }
        add_action('pre_get_posts', 'blanc_query');
    }

    //Welcart cart page row  *removing unused cells
    function blanc_cart_row($row, $cart, $materials){
        $args = compact('cart', 'i', 'cart_row', 'post_id', 'sku' );
        extract($materials);
        $row = '';
        if ( empty($options) ) {
            $optstr =  '';
            $options =  array();
        }
        $row .= '<tr>
            <td>';
            $cart_thumbnail = '<a href="' . get_permalink($post_id) . '">' . wp_get_attachment_image( $pictid, array(60, 60), true ) . '</a>';
            $row .= apply_filters('usces_filter_cart_thumbnail', $cart_thumbnail, $post_id, $pictid, $i,$cart_row);
            $row .= '</td><td>' . esc_html($cartItemName) . '<br />';
        if( is_array($options) && count($options) > 0 ){
            $optstr = '';
            foreach($options as $key => $value){
                if( !empty($key) ) {
                    $key = urldecode($key);
                    if(is_array($value)) {
                        $c = '';
                        $optstr .= esc_html($key) . ' : ';
                        foreach($value as $v) {
                            $optstr .= $c.nl2br(esc_html(urldecode($v)));
                            $c = ', ';
                        }
                        $optstr .= "<br />\n";
                    } else {
                        $optstr .= esc_html($key) . ' : ' . nl2br(esc_html(urldecode($value))) . "<br />\n";
                    }
                }
            }
            $row .= apply_filters( 'usces_filter_option_cart', $optstr, $options);
        }
        $row .= apply_filters( 'usces_filter_option_info_cart', '', $cart_row, $args );
        $row .= '</td>
            <td class="text-right">';
        if( usces_is_gptekiyo($post_id, $sku_code, $quantity) ) {
            $usces_gp = 1;
            $Business_pack_mark = '<img src="' . get_template_directory_uri() . '/images/gp.gif" alt="' . __('Business package discount','usces') . '" /><br />';
            $row .= apply_filters('usces_filter_itemGpExp_cart_mark', $Business_pack_mark);
        }
        $row .= usces_crform($skuPrice, true, false, 'return') . '
            </td>
            <td>';
        $row_quant = '<input name="quant[' . $i . '][' . $post_id . '][' . $sku . ']" class="quantity" type="text" value="' . esc_attr($cart_row['quantity']) . '" />';
        $row .= apply_filters( 'usces_filter_cart_rows_quant', $row_quant, $args );
        $row .= '</td>
            <td class="text-right">' . usces_crform(($skuPrice * $cart_row['quantity']), true, false, 'return') . '</td>
            <td class="text-center">';
        foreach($options as $key => $value){
            if(is_array($value)) {
                foreach($value as $v) {
                    $row .= '<input name="itemOption[' . $i . '][' . $post_id . '][' . $sku . '][' . $key . '][' . $v . ']" type="hidden" value="' . $v . '" />'."\n";
                }
            } else {
                $row .= '<input name="itemOption[' . $i . '][' . $post_id . '][' . $sku . '][' . $key . ']" type="hidden" value="' . $value . '" />'."\n";
            }
        }
        $row .= '<input name="itemRestriction[' . $i . ']" type="hidden" value="' . $itemRestriction . '" />
            <input name="stockid[' . $i . ']" type="hidden" value="' . $stockid . '" />
            <input name="itempostid[' . $i . ']" type="hidden" value="' . $post_id . '" />
            <input name="itemsku[' . $i . ']" type="hidden" value="' . $sku . '" />
            <input name="zaikonum[' . $i . '][' . $post_id . '][' . $sku . ']" type="hidden" value="' . esc_attr($skuZaikonum) . '" />
            <input name="skuPrice[' . $i . '][' . $post_id . '][' . $sku . ']" type="hidden" value="' . esc_attr($skuPrice) . '" />
            <input name="advance[' . $i . '][' . $post_id . '][' . $sku . ']" type="hidden" value="' . esc_attr($advance) . '" />
            <input name="delButton[' . $i . '][' . $post_id . '][' . $sku . ']" class="delButton font-awesome" type="submit" value="&#xf00d" title="' .__('Delete this item','blanc') .'" />
            </td>
        </tr>';
        return $row;
    }
    add_filter('usces_filter_cart_row', 'blanc_cart_row', 10, 3);

    //Remove unused cell in the Table on confirmation page
    function blanc_filter_confirm_row($row, $cart, $materials){
            extract($materials);
            $row = '';
            if (empty($options)) {
                $optstr =  '';
                $options =  array();
            }
            $row .= '<tr>
                <td>';
            $cart_thumbnail = wp_get_attachment_image( $pictid, array(60, 60), true );
            $row .= apply_filters('usces_filter_cart_thumbnail', $cart_thumbnail, $post_id, $pictid, $i, $cart_row);
            $row .= '</td><td>' . $cartItemName . '<br />';
            if( is_array($options) && count($options) > 0 ){
                $optstr = '';
                foreach($options as $key => $value){
                    if( !empty($key) ) {
                        $key = urldecode($key);
                        if(is_array($value)) {
                            $c = '';
                            $optstr .= esc_html($key) . ' : ';
                            foreach($value as $v) {
                                $optstr .= $c.nl2br(esc_html(urldecode($v)));
                                $c = ', ';
                            }
                            $optstr .= "<br />\n";
                        } else {
                            $optstr .= esc_html($key) . ' : ' . nl2br(esc_html(urldecode($value))) . "<br />\n";
                        }
                    }
                }
                $row .= apply_filters( 'usces_filter_option_confirm', $optstr, $options);
            }
            $row .= '</td>
                <td class="text-right">' . usces_crform($skuPrice, true, false, 'return') . '</td>
                <td class="text-center">' . $cart_row['quantity'] . '</td>
                <td class="text-right">' . usces_crform(($skuPrice * $cart_row['quantity']), true, false, 'return') . '</td>
            </tr>';
            return $row;
    }
    add_filter('usces_filter_confirm_row', 'blanc_filter_confirm_row', 10, 3);

    //SSL error fix
    //source from http://www.seshop.com/product/detail/15639/
        if( $usces->options['use_ssl'] ){
            add_action('init', 'usces_ob_start');
            function usces_ob_start(){
                global $usces;
                if( $usces->use_ssl && ($usces->is_cart_or_member_page($_SERVER['REQUEST_URI']) || $usces->is_inquiry_page($_SERVER['REQUEST_URI'])) )
                ob_start('usces_ob_callback');
            }
            if ( ! function_exists( 'usces_ob_callback' ) ) {
                function usces_ob_callback($buffer){
                global $usces;
                $pattern = array(
                    '|(<[^<]*)href=\"'.get_option('siteurl').'([^>]*)\.css([^>]*>)|',
                    '|(<[^<]*)src=\"'.get_option('siteurl').'([^>]*>)|'
                    );
                $replacement = array(
                    '${1}href="'.USCES_SSL_URL_ADMIN.'${2}.css${3}',
                    '${1}src="'.USCES_SSL_URL_ADMIN.'${2}'
                    );
                $buffer = preg_replace($pattern, $replacement, $buffer);
                return $buffer;
                }
            }
        }

}


//本体のアップデート通知を非表示
//add_filter('pre_site_transient_update_core', create_function('$a', "return  null;"));
//プラグイン更新通知を非表示
//remove_action( 'load-update-core.php', 'wp_update_plugins' );
//add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );


add_action( 'after_setup_theme', 'child_setup');
function child_setup() {
    // 子テーマフォルダ/languages下のmoファイルを読み込む
    load_child_theme_textdomain( 'blanc', get_stylesheet_directory() . '/languages');
}


/*アーカイブページを使用した場合に出るWidgetエリア*/
register_sidebar(array(
  'name' => 'アーカイブ用' ,
  'id' => 'column-blog' ,
  'before_widget' => '<div id="%1$s" class="widget %2$s">',//<div class="widget">
  'after_widget' => '</div>',
  'before_title' => '<h4 class="widgettitle">',
  'after_title' => '</h4>'
));

/*シングルページを使用した場合に出るWidgetエリア*/
register_sidebar(array(
  'name' => '投稿詳細用' ,
  'id' => 'column-single' ,
  'before_widget' => '<div id="%1$s" class="widget %2$s">',//<div class="widget">
  'after_widget' => '</div>',
  'before_title' => '<h4 class="widgettitle">',
  'after_title' => '</h4>'
));

/*問い合わせフォームのメールアドレス認証*/

add_filter( 'wpcf7_validate_email', 'wpcf7_text_validation_filter_extend', 11, 2 );
add_filter( 'wpcf7_validate_email*', 'wpcf7_text_validation_filter_extend', 11, 2 );

function wpcf7_text_validation_filter_extend( $result, $tag ) {

global $my_email_confirm;

$tag = new WPCF7_Shortcode( $tag );
$name = $tag->name;
$value = isset( $_POST[$name] )?trim( wp_unslash( strtr( (string) $_POST[$name], "\n", " " ) ) ): '';

if ($name == "your-email"){
$my_email_confirm=$value;
}
if ($name == "your-email_confirm" && $my_email_confirm != $value){
$result->invalidate( $tag,"確認用のメールアドレスが一致していません");
}

return $result;
}


add_filter( 'author_rewrite_rules', '__return_empty_array' );
function disable_author_archive() {
	if( $_GET['author'] || preg_match('#/author/.+#', $_SERVER['REQUEST_URI']) ){
		wp_redirect( home_url( '/404.php' ) );
		exit;
	}
}
add_action('init', 'disable_author_archive');


/*スライダーの背景と画像を分けるユーザー定義関数*/
function printUserData($id, $alt ,$url){
    echo '<div style="background-image=url('.$url.')"><div class="sp-slide" >'."\n";
    echo '<img src="'.$url.'" class="sp-image"'.'/>'."\n";
    echo '</div></div>'."\n";
}

/* ********************************* */
/* 201800706 追加チェックボックス　全画面用 */
/* ********************************* */
add_action('admin_menu', 'add_buttons_meta_box');
add_action('save_post', 'save_buttons_custom_fields');

// メタボックス追加
function add_buttons_meta_box() {
  add_meta_box( 'my_sectionid', '全面背景', 'buttons_custom_fields', 'page', 'side', 'high' );
}

// カスタムフィールドの入力フォーム作成と値の設定
function buttons_custom_fields() {
  global $post;
  //カスタムフィールドの値を取得
  $get_buttons = get_post_meta( $post->ID,'buttons',true );
  //値があればその値をセットし、なければ空の配列array()を設定する
  $buttons     = $get_buttons ? $get_buttons : array();
  //チェックボックス用の項目を設定
  $data      = array("full_screen_BG");
  //下記wp_nonce_fieldの_wp_nonceは、必ず別の値に書き換える。
    wp_nonce_field('wp-nonce-key', '_wp_nonce');
    foreach ( $data as $d ) {
        //取得したデータをin_array検索 $toolsの値の中に$dataと同じ物があればcheckdを付与
        if ( in_array($d, $buttons) ) { $check = "checked"; } else { $check = ""; }
        //複数選択の場合は、name=toolsではなくname=tools[]とする
      echo '<label><input type="checkbox" name="buttons[]" value="' . esc_attr($d) . '" ' . $check . '>' . esc_html($d) . '</label><br>';
  }
}

//保存用関数
function save_buttons_custom_fields($post_id) {
  if ( isset($_POST['_wp_nonce']) && $_POST['_wp_nonce'] ) {
    if ( check_admin_referer('wp-nonce-key', '_wp_nonce') ) {
      if ( isset($_POST['buttons']) && $_POST['buttons'] ) {
        update_post_meta( $post_id, 'buttons', $_POST['buttons'] );
      } else {
        delete_post_meta( $post_id, 'buttons', get_post_meta($post_id, 'buttons', true) );
      }
    }
  }
}

//全画面背景ダッシュボード制御用
function my_check() {
  echo '<script type="text/javascript" charset="utf-8">
    jQuery(document).ready(function($){
        $("#my_sectionid input").each(function(){
            if($(this).prop("checked")){
                $("#myupload3").show();
            }else{
                $("#myupload3").hide();
            }
        });
        $("#my_sectionid input").change(function(){
           if($(this).prop("checked")){
                $("#myupload3").show();
            }else{
                $("#myupload3").hide();
            } 
        });
    });
  </script>'.PHP_EOL;
}
add_action('admin_print_footer_scripts', 'my_check');

/*APPクーポン用*/
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => '3037',
		'title' => 'クーポン用フィールド',
		'fields' => array (
			array (
				'key' => 'field_597582e83a8dd',
				'label' => 'クーポン補足',
				'name' => 'repletion',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_597582f43a8de',
				'label' => 'クーポン名',
				'name' => 'title',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_597583053a8df',
				'label' => 'クーポン使用条件',
				'name' => 'use',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_597583173a8e0',
				'label' => '有効期限開始日',
				'name' => 'start',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'yy/mm/dd',
				'first_day' => 1,
			),
			array (
				'key' => 'field_5975832d3a8e1',
				'label' => '有効期限最終日',
				'name' => 'end',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'yy/mm/dd',
				'first_day' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_category',
					'operator' => '==',
					'value' => '4',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'featured_image',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}
  function hide_admin_menu() {
    global $menu;
    unset($menu[65]); // プラグイン
  }
  add_action('admin_menu', 'hide_admin_menu');
?>